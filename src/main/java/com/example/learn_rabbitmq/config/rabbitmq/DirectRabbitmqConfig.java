package com.example.learn_rabbitmq.config.rabbitmq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @program: learn_rabbitmq
 * @description: 直连型交换机
 * @author: qiu bo yang
 * @create: 2020-09-16 14:43
 **/
@Configuration
public class DirectRabbitmqConfig {

    /**
     * 队列名称：DirectQueue
     *
     * @return
     */
    @Bean
    public Queue DirectQueues() {
        //durable是否持久化,默认是false,持久化队列：会被存储在磁盘上，当消息代理重启时仍然存在，暂存队列：当前连接有效
        //exclusive:默认也是false，只能被当前创建的连接使用，而且当连接关闭后队列即被删除。此参考优先级高于durable
        //autoDelete:是否自动删除，当没有生产者或者消费者使用此队列，该队列会自动删除。

        //设置队列持久化 其余默认
        return new Queue("DirectQueue", true);
    }

    /**
     * 交换机名称：DirectExchanges
     *
     * @return
     */
    @Bean
    DirectExchange DirectExchanges() {
        return new DirectExchange("DirectExchange", true, false);
    }

    /**
     * 将队列和交换机绑定，并设置匹配键：DirectRouting
     *
     * @return
     */
    @Bean
    Binding bindingDirect() {
        return BindingBuilder.bind(DirectQueues()).to(DirectExchanges()).with("DirectRouting");
    }

    /**
     * 交换机名称：lonelyDirectExchange
     *
     * @return
     */
    @Bean
    DirectExchange lonelyDirectExchange(){
        return new DirectExchange("lonelyDirectExchange");
    }
}
