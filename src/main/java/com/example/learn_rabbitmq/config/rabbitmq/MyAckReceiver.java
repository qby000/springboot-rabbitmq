package com.example.learn_rabbitmq.config.rabbitmq;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: learn_rabbitmq
 * @description: rabbitmq手动确认模式
 * @author: qiu bo yang
 * @create: 2020-09-16 17:33
 **/
@Slf4j
@Component
public class MyAckReceiver implements ChannelAwareMessageListener {
    @Override
    public void onMessage(Message message, Channel channel) throws Exception {
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        try {
            String msg = message.toString();
            //可以点进Message里面看源码,单引号直接的数据就是我们的map消息数据
            String[] msgStr = msg.split("'");
            Map<String, String> msgMap = mapStringToMap(msgStr[1].trim(), 3);

            if ("DirectQueue".equals(message.getMessageProperties().getConsumerQueue())) {
                log.info("消费的消息来自的队列名：{},messageId：{},messageData：{},createTime：{}", message.getMessageProperties().getConsumerQueue(), msgMap.get("messageId"), msgMap.get("messageData"), msgMap.get("createTime"));
                System.out.println("执行DirectQueue中的消息的业务处理流程......");

            }

            if ("fanout.A".equals(message.getMessageProperties().getConsumerQueue())) {
                log.info("消费的消息来自的队列名：{},messageId：{},messageData：{},createTime：{}", message.getMessageProperties().getConsumerQueue(), msgMap.get("messageId"), msgMap.get("messageData"), msgMap.get("createTime"));
                System.out.println("执行fanout.A中的消息的业务处理流程......");
            }

            if ("fanout.B".equals(message.getMessageProperties().getConsumerQueue())) {
                log.info("消费的消息来自的队列名：{},messageId：{},messageData：{},createTime：{}", message.getMessageProperties().getConsumerQueue(), msgMap.get("messageId"), msgMap.get("messageData"), msgMap.get("createTime"));
                System.out.println("执行fanout.B中的消息的业务处理流程......");
            }

            if ("fanout.C".equals(message.getMessageProperties().getConsumerQueue())) {
                log.info("消费的消息来自的队列名：{},messageId：{},messageData：{},createTime：{}", message.getMessageProperties().getConsumerQueue(), msgMap.get("messageId"), msgMap.get("messageData"), msgMap.get("createTime"));
                System.out.println("执行fanout.C中的消息的业务处理流程......");
            }

            if ("topic.man".equals(message.getMessageProperties().getConsumerQueue())) {
                log.info("消费的消息来自的队列名：{},messageId：{},messageData：{},createTime：{}", message.getMessageProperties().getConsumerQueue(), msgMap.get("messageId"), msgMap.get("messageData"), msgMap.get("createTime"));
                System.out.println("执行topic.man中的消息的业务处理流程......");
            }

            if ("topic.woman".equals(message.getMessageProperties().getConsumerQueue())) {
                log.info("消费的消息来自的队列名：{},messageId：{},messageData：{},createTime：{}", message.getMessageProperties().getConsumerQueue(), msgMap.get("messageId"), msgMap.get("messageData"), msgMap.get("createTime"));
                System.out.println("执行topic.woman中的消息的业务处理流程......");
            }
            channel.basicAck(deliveryTag, true);
            //为true会重新放回队列
            //channel.basicReject(deliveryTag, true);
        } catch (Exception e) {
            channel.basicReject(deliveryTag, false);
            e.printStackTrace();
        }
    }

    /**
     * {key=value,key=value,key=value} 格式转换成map
     *
     * @param str
     * @param entryNum
     * @return
     */
    private Map<String, String> mapStringToMap(String str, int entryNum) {
        str = str.substring(1, str.length() - 1);
        String[] strings = str.split(",", entryNum);
        Map<String, String> map = new HashMap<>();
        for (String string : strings) {
            String key = string.split("=")[0].trim();
            String value = string.split("=")[1];
            map.put(key, value);
        }
        return map;
    }
}
