package com.example.learn_rabbitmq.config.rabbitmq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @program: learn_rabbitmq
 * @description: 主题交换机
 * @author: qiu bo yang
 * @create: 2020-09-16 15:33
 **/
@Configuration
public class TopicRabbitmqConfig {
    //绑定键
    public final static String man = "topic.man";
    public final static String woman = "topic.woman";

    /**
     * 队列名称：topic.man
     *
     * @return
     */
    @Bean
    public Queue manQueue() {
        return new Queue(TopicRabbitmqConfig.man);
    }

    /**
     * 队列名称：topic.woman
     *
     * @return
     */
    @Bean
    public Queue womanQueue() {
        return new Queue(TopicRabbitmqConfig.woman);
    }

    /**
     * 交换机名称：topicExchange
     *
     * @return
     */
    @Bean
    TopicExchange exchange() {
        return new TopicExchange("topicExchange");
    }

    /**
     * 绑定的键值：topic.man
     * 消息携带的路由键是topic.man,才会分发到该队列
     *
     * @return
     */
    @Bean
    Binding bindingExchangeMessageMan() {
        return BindingBuilder.bind(manQueue()).to(exchange()).with(man);
    }

    /**
     * 绑定的键值：通配路由键规则topic.#
     * 消息携带的路由键是以topic.开头,都会分发到该队列
     *
     * @return
     */
    @Bean
    Binding bindingExchangeMessageWoman() {
        return BindingBuilder.bind(womanQueue()).to(exchange()).with("topic.#");
    }
}
