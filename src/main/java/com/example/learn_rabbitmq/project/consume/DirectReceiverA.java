package com.example.learn_rabbitmq.project.consume;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @program: learn_rabbitmq
 * @description: Direct消费者
 * @author: qiu bo yang
 * @create: 2020-09-16 15:22
 **/
@Slf4j
@Component
@RabbitListener(queues = "DirectQueue") //监听队列名称：DirectQueue
public class DirectReceiverA {
    /**
     * 消费者收到消息
     *
     * @param map
     */
    @RabbitHandler
    public void process(Map<String, Object> map) {
        log.info("消费者收到消息A：{}", map);
    }
}
