package com.example.learn_rabbitmq.project.consume;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @program: learn_rabbitmq
 * @description: 订阅消费者
 * @author: qiu bo yang
 * @create: 2020-09-16 16:01
 **/
@Slf4j
@Component
@RabbitListener(queues = "topic.man")
public class TopicManReceiver {
    /**
     * 消费者收到消息
     *
     * @param map
     */
    @RabbitHandler
    public void process(Map<String, Object> map) {
        log.info("订阅消费者man：{}", map);
    }
}
