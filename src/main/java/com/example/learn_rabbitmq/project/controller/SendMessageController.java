package com.example.learn_rabbitmq.project.controller;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @program: learn_rabbitmq
 * @description: 消息推送业务层
 * @author: qiu bo yang
 * @create: 2020-09-16 14:59
 **/
@RestController
@RequestMapping("/sendMessage")
public class SendMessageController {

    //使用RabbitTemplate,这提供了接收/发送等等方法
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送消息推送业务(直连型消息推送)
     *
     * @param message 业务消息数据
     * @return
     */
    @PostMapping("/sendDirectMessage")
    public String sendDirectMessage(String message) {

        Map<String, Object> map = doSendMessage(message);
        //将消息携带绑定键值：DirectRouting 发送到交换机DirectExchanges
        rabbitTemplate.convertAndSend("DirectExchange", "DirectRouting", map);
        return "ok";
    }

    /**
     * 推送消息到主题交换机
     *
     * @param message
     * @return
     */
    @PostMapping("/sendTopicMessageMan")
    public String sendTopicMessageMan(String message) {
        Map<String, Object> map = doSendMessage(message);
        rabbitTemplate.convertAndSend("topicExchange", "topic.man", map);
        return "ok";
    }

    /**
     * 推送消息到主题交换机
     *
     * @param message
     * @return
     */
    @PostMapping("/sendTopicMessageWoman")
    public String sendTopicMessageWoman(String message) {
        Map<String, Object> map = doSendMessage(message);
        rabbitTemplate.convertAndSend("topicExchange", "topic.woman", map);
        return "ok";
    }

    /**
     * 测试消息确认
     *
     * @param message
     * @return
     */
    @PostMapping("/sendAckMessage")
    public String sendAckMessage(String message) {
        Map<String, Object> map = doSendMessage(message);
        rabbitTemplate.convertAndSend("lonelyDirectExchange", "DirectRouting", map);
        return "ok";
    }

    /**
     * 消息发送业务封装
     *
     * @param message
     * @return
     */
    private Map<String, Object> doSendMessage(String message) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", message);
        map.put("messageId", UUID.randomUUID());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        map.put("createTime", sdf.format(new Date()));
        return map;
    }
}
